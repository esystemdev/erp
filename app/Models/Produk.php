<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    public $table = 'produk';

    protected $fillable = [
        'nama_produk',
        'kode_barang',
        'id_kategori',
        'jenis',
        'unit',
        'harga',
        'keterangan'
    ];

    public function stock()
    {
        return $this->hasMany('App\Models\ProdukDetail', 'id_produk');
    }

    public function gambar()
    {
        return $this->hasMany('App\Models\ProdukGambar', 'id_produk');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Models\Kategori', 'id_kategori');
    }
}
