<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdukDetail extends Model
{
    public $table = 'produk_details';

    protected $fillable = [
        'id_produk',
        'id_cabang',
        'kuantitas',
    ];

    public function produk()
    {
        return $this->belongsTo('App\Models\Produk', 'id_produk');
    }

    public function cabang()
    {
        return $this->belongsTo('App\Models\Cabang', 'id_cabang');
    }
}
