<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $fillable = [
        'id_user',
        'no_phone',
        'address'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'id');
    }
}
