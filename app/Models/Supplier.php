<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public $table = 'supplier';

    protected $fillable = [
        'nama_supplier',
        'no_kontak',
        'keterangan'
    ];
}
