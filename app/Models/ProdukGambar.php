<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdukGambar extends Model
{
    public $table = 'produk_gambars';

    protected $fillable = [
        'id_produk',
        'file_path'
    ];

    public function produk()
    {
        return $this->belongsTo('App\Models\Produk', 'id_produk');
    }
}
