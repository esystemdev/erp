<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    public $table = 'pelanggans';

    protected $fillable = [
        'nama_pelanggan',
        'no_kontak',
        'alamat',
        'no_pid',
        'keterangan'
    ];
}
