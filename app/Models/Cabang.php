<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
    public $table = 'cabangs';

    protected $fillable = [
        'nama_cabang',
        'no_kontak',
        'alamat',
        'keterangan'
    ];
}
