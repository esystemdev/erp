<?php

namespace App\Helpers;

use App\User;
use Illuminate\Support\Facades\Auth;

class Customs
{
    public function getUserBy($id=null)
    {
        if($id==null) {
            $id = Auth::user()->id;
        }

        return User::with(['profile'])->find($id);
    }
}
