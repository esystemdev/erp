<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class AuthController extends ApiController
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return $this->apiResponse('Invalid Credentials', false, null, 400 );
            }
        } catch (JWTException $e) {
            return $this->apiResponse('Could not create token', false, $e, 500 );
        }

        return $this->apiResponse('Token create successfull', true, compact('token'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|string',
            'username' => 'required|string|max:100|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string',
            'roles' => 'required|string'
        ]);

        if($validator->fails()){
            return $this->apiResponse('Registration failed', false,$validator->errors()->toJson(), 400);
        }

        $user = User::create([
            'fullname' => $request->get('fullname'),
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        $user->roles()->attach(Role::where('slug', $request->get('roles'))->first());

        $token = JWTAuth::fromUser($user);

        return $this->apiResponse('Registration successfully', true, compact('user','token'), 201);
    }


    public function getAuthenticatedUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return $this->apiResponse('User not found', false, null, 404);
            }
        }
        catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return $this->apiResponse('Token expired', false, null, $e->getStatusCode());
        }
        catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return $this->apiResponse('Token invalid', false, null, $e->getStatusCode());
        }
        catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return $this->apiResponse('Token absent', false, null, $e->getStatusCode());
        }

        return $this->apiResponse('User identified', true, compact('user'));
    }


}
