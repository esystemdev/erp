<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

class KategoriApiController extends ApiController
{
    protected $user;

    public function __constructor()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function getCategoryBy($id)
    {
        $category = Kategori::find($di);
        if(!$category) {
            return $this->apiResponse(
                "Not Found Data Category",
                false,
                []
            );
        }

        return $this->apiResponse("Found Data Category",
            true,
            [
                "kategori" => $kategori,
                "user" => $this->user
            ]
        );
    }
}
