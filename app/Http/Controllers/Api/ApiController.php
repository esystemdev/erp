<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function apiResponse($message, $success, $data = null, $code = 200, $headers = null)
    {
        $response = [
            'success' => $success,
            'message' => $message,
        ];

        if($data != null) {
            $response['data'] = $data;
        }

        if($headers != null)
        {
            return response()->json($response, $code, $headers);
        }
        else
        {
            return response()->json($response, $code);
        }

    }
}
