<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;

class ViewController extends BaseController
{
    /** CONSTRUCTOR */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /** DASHBOARD */
    public function dashboard()
    {
        $records = [
            'user' => $this->customs()->getUserBy(),
            'title' => 'dashboard'
        ];
        return view('pages/dashboard', compact('records'));
    }

    /** KATEGORI VIEW*/
    public function kategori()
    {
        $records = [
            'user'  => $this->customs()->getUserBy(),
            'title' => 'kategori'
        ];
        return view('pages/kategori/index', compact('records'));
    }


    /** SUPPLIER VIEW*/
    public function supplier()
    {
        $records = [
            'user'  => $this->customs()->getUserBy(),
            'title' => 'supplier'
        ];
        return view('pages/supplier/index', compact('records'));
    }


    /** CABANG VIEW */
    public function cabang()
    {
        $records = [
            'user'  => $this->customs()->getUserBy(),
            'title' => 'cabang'
        ];
        return view('pages/cabang/index', compact('records'));
    }

    /** PRODUK VIEW */
    public function produk()
    {
        $records = [
            'user'  => $this->customs()->getUserBy(),
            'title' => 'produk'
        ];
        return view('pages/produk/index', compact('records'));
    }

}
