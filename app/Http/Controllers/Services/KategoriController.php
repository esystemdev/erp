<?php

namespace App\Http\Controllers\Services;

use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;

class KategoriController extends BaseController
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    //Find ID
    public function findKategori($id)
    {
        try {
            $record = Kategori::find($id);
            return  $this->sendResponse("Data kategori ditemukan", true, $record, 200);
        }
        catch (QueryException $e) {
            return $this->sendResponse("Gagal koneksi ke server", false, $e->errorInfo, 400);
        }
    }

    //GET ALL
    public function allKategori()
    {
        try {
            $records = Kategori::all();
            return  $this->sendResponse("Data seluruh kategori berhasil ditarik", true, $records, 200);
        }
        catch (QueryException $e) {
            return $this->sendResponse("Gagal koneksi ke server", false, $e->errorInfo, 400);
        }
    }

    //Insert
    public function insertKategori(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_kategori' => 'required|unique:kategori,nama_kategori',
        ]);

        if($validator->fails()) {
            return $this->sendResponse('Gagal validasi form', false, $validator->errors(), 401);
        }

        $inserted = Kategori::create([
            'nama_kategori' => $request->nama_kategori,
            'keterangan_kategori' => $request->keterangan_kategori
        ]);

        if($inserted)
        {
            return $this->sendResponse('Data kategori berhasil disimpan', true, $inserted, 200 );
        }
        else {
            return $this->sendResponse( 'Data kategori gagal disimpan. Mohon periksa koneksi server anda',  false, null, 400 );
        }
    }

    //Update
    public function updateKategori(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_kategori' => 'required',
        ]);

        if($validator->fails()) {
            return $this->sendResponse('Gagal validasi form', false, $validator->errors(), 401);
        }

        $records = Kategori::find($id);

        if(!$records) {
            return $this->sendResponse('Id Kategori '. $id . ' tidak ditemukan', false, null, 200);
        }

        if($records->nama_kategori != $request->nama_kategori) {
            $found = Kategori::where('nama_kategori', $request->nama_kategori)->get();

            if(count($found) > 0) {
                return $this->sendResponse('Nama kategori '. $request->nama_kategori . ' sudah ada. Mohon periksa kembali', false, null, 200);
            }
        }

        $records->nama_kategori = $request->nama_kategori;
        $records->keterangan_kategori = $request->keterangan_kategori;
        $records->save();
        return $this->sendResponse('Data kategori berhasil diperbarui', true, $records, 202);

    }

    //Delete
    public function deleteKategori($id)
    {
        try{
            $record = Kategori::find($id);
            $name = $record->nama_kategori;
            $record->delete();
            return $this->sendResponse("Kategori ".$name. " berhasil dihapus", true, null, 200);
        }
        catch (QueryException $e) {
            return $this->sendResponse("Gagal koneksi ke server", false, $e->errorInfo, 400);
        }
    }

}
