<?php

namespace App\Http\Controllers\Services;

use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;

class ProdukController extends BaseController
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    //Find ID
    public function findProduk($id)
    {
        try {
            $record = Produk::find($id);
            return  $this->sendResponse("Data produk ditemukan", true, $record, 200);
        }
        catch (QueryException $e) {
            return $this->sendResponse("Gagal koneksi ke server", false, $e->errorInfo, 400);
        }
    }

    //GET ALL
    public function allProduk()
    {
        try {
            $records = Produk::all();
            return  $this->sendResponse("Data seluruh produk berhasil ditarik", true, $records, 200);
        }
        catch (QueryException $e) {
            return $this->sendResponse("Gagal koneksi ke server", false, $e->errorInfo, 400);
        }
    }

    //Insert
    public function insertProduk(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_produk' => 'required|unique:produk,nama_produk',
        ]);

        if($validator->fails()) {
            return $this->sendResponse('Gagal validasi form', false, $validator->errors(), 401);
        }

        $inserted = Produk::create([
            'nama_produk' => $request->nama_produk,
            'keterangan_produk' => $request->keterangan_produk
        ]);

        if($inserted)
        {
            return $this->sendResponse('Data produk berhasil disimpan', true, $inserted, 200 );
        }
        else {
            return $this->sendResponse( 'Data produk gagal disimpan. Mohon periksa koneksi server anda',  false, null, 400 );
        }
    }

    //Update
    public function updateProduk(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_produk' => 'required',
        ]);

        if($validator->fails()) {
            return $this->sendResponse('Gagal validasi form', false, $validator->errors(), 401);
        }

        $records = Produk::find($id);

        if(!$records) {
            return $this->sendResponse('Id Produk '. $id . ' tidak ditemukan', false, null, 200);
        }

        if($records->nama_produk != $request->nama_produk) {
            $found = Produk::where('nama_produk', $request->nama_produk)->get();

            if(count($found) > 0) {
                return $this->sendResponse('Nama produk '. $request->nama_produk . ' sudah ada. Mohon periksa kembali', false, null, 200);
            }
        }

        $records->nama_produk = $request->nama_produk;
        $records->keterangan_produk = $request->keterangan_produk;
        $records->save();
        return $this->sendResponse('Data produk berhasil diperbarui', true, $records, 202);

    }

    //Delete
    public function deleteProduk($id)
    {
        try{
            $record = Produk::find($id);
            $name = $record->nama_produk;
            $record->delete();
            return $this->sendResponse("Produk ".$name. " berhasil dihapus", true, null, 200);
        }
        catch (QueryException $e) {
            return $this->sendResponse("Gagal koneksi ke server", false, $e->errorInfo, 400);
        }
    }

}
