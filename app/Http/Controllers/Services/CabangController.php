<?php

namespace App\Http\Controllers\Services;

use App\Models\Cabang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;

class CabangController extends BaseController
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    //Find ID
    public function findCabang($id)
    {
        try {
            $record = Cabang::find($id);
            return  $this->sendResponse("Data cabang ditemukan", true, $record, 200);
        }
        catch (QueryException $e) {
            return $this->sendResponse("Gagal koneksi ke server", false, $e->errorInfo, 400);
        }
    }

    //GET ALL
    public function allCabang()
    {
        try {
            $records = Cabang::all();
            return  $this->sendResponse("Data seluruh cabang berhasil ditarik", true, $records, 200);
        }
        catch (QueryException $e) {
            return $this->sendResponse("Gagal koneksi ke server", false, $e->errorInfo, 400);
        }
    }

    //Insert
    public function insertCabang(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_cabang' => 'required|unique:cabang,nama_cabang',
        ]);

        if($validator->fails()) {
            return $this->sendResponse('Gagal validasi form', false, $validator->errors(), 401);
        }

        $inserted = Cabang::create([
            'nama_cabang' => $request->nama_cabang,
            'keterangan_cabang' => $request->keterangan_cabang
        ]);

        if($inserted)
        {
            return $this->sendResponse('Data cabang berhasil disimpan', true, $inserted, 200 );
        }
        else {
            return $this->sendResponse( 'Data cabang gagal disimpan. Mohon periksa koneksi server anda',  false, null, 400 );
        }
    }

    //Update
    public function updateCabang(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_cabang' => 'required',
        ]);

        if($validator->fails()) {
            return $this->sendResponse('Gagal validasi form', false, $validator->errors(), 401);
        }

        $records = Cabang::find($id);

        if(!$records) {
            return $this->sendResponse('Id Cabang '. $id . ' tidak ditemukan', false, null, 200);
        }

        if($records->nama_cabang != $request->nama_cabang) {
            $found = Cabang::where('nama_cabang', $request->nama_cabang)->get();

            if(count($found) > 0) {
                return $this->sendResponse('Nama cabang '. $request->nama_cabang . ' sudah ada. Mohon periksa kembali', false, null, 200);
            }
        }

        $records->nama_cabang = $request->nama_cabang;
        $records->keterangan_cabang = $request->keterangan_cabang;
        $records->save();
        return $this->sendResponse('Data cabang berhasil diperbarui', true, $records, 202);

    }

    //Delete
    public function deleteCabang($id)
    {
        try{
            $record = Cabang::find($id);
            $name = $record->nama_cabang;
            $record->delete();
            return $this->sendResponse("Cabang ".$name. " berhasil dihapus", true, null, 200);
        }
        catch (QueryException $e) {
            return $this->sendResponse("Gagal koneksi ke server", false, $e->errorInfo, 400);
        }
    }

}
