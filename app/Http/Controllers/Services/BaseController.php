<?php

namespace App\Http\Controllers\Services;

use App\Helpers\Customs;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{

    public function customs()
    {
        return new Customs();
    }

    public function sendResponse($message, $success, $data = null, $code = 200, $headers = null)
    {
        $response = [
            'success' => $success,
            'message' => $message,
        ];

        if($data != null) {
            $response['data'] = $data;
        }

        if($headers != null)
        {
            return response()->json($response, $code, $headers);
        }
        else
        {
            return response()->json($response, $code);
        }

    }
}
