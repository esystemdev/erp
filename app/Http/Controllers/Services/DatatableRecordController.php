<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;


use App\Models\Kategori;
use App\Models\Cabang;

class DatatableRecordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //KATEGORI
    public function getKategoriDatatable(Request $request)
    {
        $records = Kategori::get();
        return DataTables::of($records)->make(true);
    }

    //CABANG
    public function getCabangDatatable(Request $request)
    {
        $records = Cabang::get();
        return DataTables::of($records)->make(true);
    }


    //SUPPLIER
    public function getSupplierDatatable(Request $request)
    {
        //
    }
}
