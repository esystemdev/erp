<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <link href="{{ asset('templates/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('templates/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('templates/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('templates/js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">
    <link href="{{ asset('templates/jquery-confirm/dist/jquery-confirm.min.css') }}" rel="stylesheet">
    <link href="{{ asset('templates/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('templates/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('templates/css/style.css') }}" rel="stylesheet">
    <style type="text/css">
        .foto-profile {
            width:48px;
            height:48px;
        }

        .executive-color {
            background-color: #f7f7f7;
            color: #0d7ea0;
        }
    </style>

</head>
<body>

<div id="wrapper">

    @include('partials.right_nav', ['user' => $records['user'], 'title' => $records['title'] ])

    <div id="page-wrapper" class="gray-bg">
        @include('partials.top_nav', [ 'user' => $records['user'] ])

        @yield('content')


        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>

        @include('partials.footer')
    </div>

</div>


<script src="{{ asset('templates/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('templates/axios-master/dist/axios.min.js') }}"></script>
<script src="{{ asset('templates/js/popper.min.js') }}"></script>
<script src="{{ asset('templates/js/bootstrap.js') }}"></script>
<script src="{{ asset('templates/jquery-confirm/dist/jquery-confirm.min.js') }}"></script>
<script src="{{ asset('templates/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('templates/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('templates/js/plugins/flot/jquery.flot.js') }}"></script>
<script src="{{ asset('templates/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ asset('templates/js/plugins/flot/jquery.flot.spline.js') }}"></script>
<script src="{{ asset('templates/js/plugins/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('templates/js/plugins/flot/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('templates/js/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('templates/js/demo/peity-demo.js') }}"></script>

<script src="{{ asset('templates/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('templates/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ asset('templates/jquery_validation/dist/jquery.validate.min.js') }}"></script>
<script src="{{ asset('templates/jquery_validation/dist/additional-methods.min.js') }}"></script>

<script src="{{ asset('templates/js/inspinia.js') }}"></script>
<script src="{{ asset('templates/js/plugins/pace/pace.min.js') }}"></script>
<script src="{{ asset('templates/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('templates/js/plugins/gritter/jquery.gritter.min.js') }}"></script>
<script src="{{ asset('templates/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('templates/js/demo/sparkline-demo.js') }}"></script>
<script src="{{ asset('templates/js/plugins/chartJs/Chart.min.js') }}"></script>
<script src="{{ asset('templates/js/plugins/toastr/toastr.min.js') }}"></script>

<script src="{{ asset('js/libraries.js') }}"></script>

<script>
var APP_URL = {!! json_encode(url('/')) !!}
axios.defaults.baseURL = APP_URL
</script>
@yield('scripts')

</body>
</html>
