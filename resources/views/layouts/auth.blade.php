<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ asset('templates/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/auth.css') }}" />

</head>
<body>


<div class="wrapper">
    <div class="container cover-container">
        <div class="row justify-content-center">
            @yield('content')
        </div>
    </div>
</div>



<script src="{{ asset('templates/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('templates/js/bootstrap.min.js') }}"></script>

</body>
</html>

