@extends('layouts.auth')

@section('title')
    ERP LOGIN
@endsection

@section('content')
<div class="col-md-6 col-sm-6 box-form">

    <div class="col-md-12 text-center">
        <h5 class="text-primary">LOGIN ACCOUNT</h5>
    </div>

    <div class="col-12">&nbsp;</div>

    <div class="col-12">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group row">
                <label for="username" class="col-4 col-form-label text-md-right">{{ __('Username') }}</label>

                <div class="col-8">
                    <input id="username" name="username" type="text" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" required autocomplete="username" autofocus>

                    @error('username')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-4 col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="col-8">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-8 offset-4">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Ingatkan saya') }}
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-8 offset-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Login') }}
                    </button>

                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Lupa password ?') }}
                        </a>
                    @endif
                </div>
            </div>
        </form>
    </div>

</div>
@endsection
