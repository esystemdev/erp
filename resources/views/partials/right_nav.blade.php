<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                @if ($user->profile->foto_profile != null)
                    <img alt="image" class="rounded-circle foto-profile" src="{{ url('app/public'. $user->profile->foto_profile) }}"/>
                    @else
                    <img alt="image" class="rounded-circle foto-profile" src="{{ url('img/user/default.jpg')}}" />
                @endif

                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold">
                            {{ $user->fullname }}
                        </span>
                        <span class="text-muted text-xs block">
                            {{ $records['user']->roles[0]->name }} <b class="caret"></b>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li>
                            <a class="dropdown-item" href="{{ url('/view/profil') }}">Profile</a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <!-- Dashboard -->
            <li class="{{ ($title == 'dashboard') ? 'active' : '' }}">
                <a href="{{ url('/view/dashboard') }}">
                    <i class="fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>

            <!-- Master User (Pengguna) -->
            <li>
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span class="nav-label">Pengguna</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="graph_flot.html">Staff/Karyawan</a></li>
                    <li><a href="graph_morris.html">Pelanggan</a></li>
                </ul>
            </li>

            <!-- Master Kategori -->
            <li class="{{ ($title == 'kategori') ? 'active' : '' }}">
                <a href="{{ url('view/kategori') }}">
                    <i class="fa fa-list-ul"></i>
                    <span class="nav-label">Kategori</span>
                </a>
            </li>

            <!-- Master Cabang -->
            <li class="{{ ($title == 'cabang') ? 'active' : '' }}">
                <a href="{{ url('view/cabang') }}">
                    <i class="fa fa-code-fork"></i>
                    <span class="nav-label">Cabang</span>
                </a>
            </li>

            <!-- Master Supplier -->
            <li class="{{ ($title == 'supplier') ? 'active' : '' }}">
                <a href="{{ url('view/supplier') }}">
                    <i class="fa fa-industry"></i>
                    <span class="nav-label">Supplier</span>
                </a>
            </li>


            <!-- Master Produk -->
            <li class="{{ ($title == 'produk') ? 'active' : '' }}">
                <a href="{{ url('view/produk') }}">
                    <i class="fa fa-product-hunt"></i>
                    <span class="nav-label">Produk</span>
                </a>
            </li>

        </ul>

    </div>
</nav>
