<div class="modal inmodal" id="modalBoxCategory" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Form Kategori</h4>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <form id="modalFormCategory" class="m-t-md">
                        <div class="form-group row">
                            <label for="modalTextNama" class="col-sm-3 col-lg-4 col-form-label">
                                Nama Kategori
                            </label>
                            <div class="col-sm-9 col-lg-8">
                                <input type="text" id="modalTextNama" name="modalTextNama" class="form-control" value=""/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="modalTextKeterangan" class="col-sm-3 col-lg-4 col-form-label">
                                Keterangan
                            </label>
                            <div class="col-sm-9 col-lg-8">
                                <textarea class="form-control" id="modalTextKeterangan" name="modalTextKeterangan" rows="4"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" id="modalButtonSave" class="btn btn-primary" data-id="">
                    <i class="fa fa-save"></i> SIMPAN
                </button>
            </div>
        </div>
    </div>
</div>
