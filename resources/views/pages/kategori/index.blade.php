@extends('layouts.web')

@section('title')
    ERP KATEGORI
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2 class="text-success">
            <strong>Kategori Barang (Produk)</strong>
        </h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="javascript:void(0)" onclick="addNewRow()">
                    <i class="fa fa-plus"></i> Tambah Kategori Baru
                </a>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5 class="text-primary">Record Data Kategori</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table id="table_list" class="table table-bordered">
                            <thead class="executive-color">
                                <tr class="text-center align-middle">
                                    <th>No.</th>
                                    <th>Nama Kategori</th>
                                    <th>Keterangan Kategori</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@include('pages.modals.box_kategori')
@endsection


@section('scripts')
<script src="{{ asset('clients/master_categories.js') }}"></script>
<script>
var records = {!! json_encode($records, JSON_HEX_TAG) !!}

$(function () {
    refreshFormModal();
    getDatatableCategory(table_list)

    //Save Data
    modalButtonSave.click(function (e) {
        e.preventDefault();

        var id = $(this).attr("data-id");

        modalFormCategory.validate({
            rules: {
                modalTextNama: {required: true }
            },
            messages: {
                modalTextNama: {required: "Nama kategori wajib diisi" }
            },
        });

        if(modalFormCategory.valid())
        {
            let categories = new Categories(modalTextNama.val(), modalTextKeterangan.val(), id);

            if(id != '' && id != null) {
                categories.updateRow('/kategori/update');
            }
            else {
                categories.insertRow('/kategori/insert');

            }


        }

    });

});

</script>
@endsection
