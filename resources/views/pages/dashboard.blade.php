@extends('layouts.web')

@section('title')
    ERP DASHBOARD
@endsection

@section('content')
<div class="row border-bottom white-bg dashboard-header">
    <div class="col-md-12">
        <h2>Segera Aktif</h2>
        <small>Beberapa Item Akan Segera diaktifkan. Setelah Fix Production</small>
        <ul class="list-group clear-list m-t">
            <li class="list-group-item fist-item">
                <span class="label label-success">1</span>
                Data Barang dan Detail Barang
            </li>
            <li class="list-group-item">
                <span class="label label-info">2</span>
                Data Supplier dan Detail Barang Supplier
            </li>
            <li class="list-group-item">
                <span class="label label-primary">3</span>
                Data Kategori Barang
            </li>
            <li class="list-group-item">
                <span class="label label-default">4</span>
                Data Transaksi Penjualan
            </li>
            <li class="list-group-item">
                <span class="label label-primary">5</span>
                Data Stok Barang Gudang
            </li>
        </ul>
    </div>
</div>
@endsection


@section('scripts')
<script></script>
@endsection
