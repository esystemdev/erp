@extends('layouts.web')

@section('title')
    ERP CABANG
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2 class="text-success">
            <strong>Cabang (Ragam Toko dan Gudang)</strong>
        </h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="javascript:void(0)" onclick="addNewRow()">
                    <i class="fa fa-plus"></i> Tambah Cabang Baru
                </a>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5 class="text-primary">Record Data Kategori</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                        <table id="table_list" class="table table-bordered">
                            <thead class="executive-color">
                                <tr class="text-center align-middle">
                                    <th>No.</th>
                                    <th>Nama Cabang</th>
                                    <th>No. Kontak Kategori</th>
                                    <th>Alamat (Lokasi)</th>
                                    <th>Keterangan</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@include('pages.modals.box_cabang')
@endsection


@section('scripts')
<script src="{{ asset('clients/master_cabang.js') }}"></script>
<script>
var records = {!! json_encode($records, JSON_HEX_TAG) !!}

$(function () {
    refreshFormModal();
    getDatatableCabang(table_list)

     //Save Data
    modalButtonSave.click(function (e) {
        e.preventDefault();

        var id = $(this).attr("data-id");

        modalFormCabang.validate({
            rules: {
                modalTextNama: { required: true },
                modalTextKontak: { required: true },
                modalTextAlamat: { required: true }
            },
            messages: {
                modalTextNama: {required: "Nama cabang wajib diisi" },
                modalTextKontak: {required: "No kontak cabang wajib diisi" },
                modalTextAlamat: {required: "Alamat (lokasi) cabang wajib diisi" }
            },
        });

        if(modalFormCabang.valid())
        {
            let results = new Cabang(
                modalTextNama.val(),
                modalTextKontak.val(),
                modalTextAlamat.val(),
                modalTextKeterangan.val(),
                id
            );

            if(id != '' && id != null) {
                results.updateRow('/kategori/update');
            }
            else {
                results.insertRow('/kategori/insert');

            }
        }

    });


});

</script>
@endsection
