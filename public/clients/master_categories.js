'use strict'

var table_list = "table_list";
var modalBoxCategory = $("#modalBoxCategory");
var modalFormCategory = $("#modalFormCategory");
var modalButtonSave = $("#modalButtonSave");
var modalTextNama = $("#modalTextNama");
var modalTextKeterangan = $("#modalTextKeterangan");

function refreshFormModal(str = 'add')
{
    if(str == 'add') {
        $('.modal-title').text('Form Tambah Kategori')
    }
    else {
        $('.modal-title').text('Form Ubah Kategori')
    }
    modalTextNama.val("");
    modalTextKeterangan.val("");
    modalButtonSave.attr("data-id", "");
    modalBoxCategory.modal('hide');
}

//Add Category
function addNewRow()
{
    refreshFormModal();
    modalButtonSave.text("");
    modalButtonSave.append('<i class="fa fa-save"></i> SIMPAN')
    modalBoxCategory.modal('show');
}

//Edit Category
function editRowData(id)
{
    refreshFormModal('edit');
    axios.get("/kategori/found/" + id).then((response) => {
        console.log(response);
        if(response.data.success == true && response.status == 200)
        {
            let data = response.data.data;
            modalTextNama.val(data.nama_kategori);
            modalTextKeterangan.val(data.keterangan_kategori);
        }

    }).catch((error) => {
        console.log(error.response)
    })

    modalButtonSave.text("");
    modalButtonSave.append('<i class="fa fa-save"></i> UBAH')
    modalButtonSave.attr("data-id", id)
    modalBoxCategory.modal('show');
}

//Delete Category
function deleteRowData(id)
{
    confirmBox('KONFIRMASI', 'Apakah anda yakin ingin menghapus data kategori ini ?', 'fa fa-check-square-o', 'blue', function(){
        axios.delete('/kategori/delete/' + id).then((response) => {
            console.log(response.data)
            if(response.status == 200 && response.data.success == true)
            {
                alertBox("BERHASIL", response.data.message, "fa fa-check", "green", function() {
                    getDatatableCategory(table_list);
                });
            }
        }).catch((error) => {
            console.log(error.response)
        })
    }, function() {})
}


function getDatatableCategory (tableName, _column_def = null, _filters = null)
{

    tableDestroyed(tableName)

    let d_tables = $("#" + tableName).DataTable({
        processing: true,
        serverSide: true,
        searching: true,
        autoWidth: false,
        ordering: true,
        responsive: true,
        lengthChange: true,
        ajax: {
            url: APP_URL + "/record/kategori/data",
            data: function (d) {
                d.filters = _filters;
            }
        },
        columnDefs: (_column_def == null) ? [] : _column_def,
        columns: [
            {
                className: 'text-center align-middle',
                defaultContent: '',
            },
            {
                data: "nama_kategori",
                name: "nama_kategori",
                className: "align-middle",
            },
            {
                data: "keterangan_kategori",
                name: "keterangan_kategori",
                className: "align-middle",
            },
            {
                className: "text-center align-middle",
                defaultContent: "",
            }
        ],
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'excel_kategori'},
            {extend: 'pdf', title: 'excel_pdf'},

            {extend: 'print', customize: function (win){
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
            }}
        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var str = ''
            str += '<button type="button" class="btn btn-sm btn-success" onclick="editRowData(\'' + aData.id + "')\">";
                str += '<i class="fa fa-edit"></i> Ubah';
            str += "</button>&nbsp;";

            str += '<button type="button" class="btn btn-sm btn-danger" onclick="deleteRowData(\'' + aData.id + "')\">";
                str += '<i class="fa fa-trash"></i> Hapus';
            str += "</button>";

            $("td:eq(3)", nRow).html(str);
        }
    });

    d_tables.on('xhr', function (e, settings, json) {
        console.log(json);
        console.log(settings);
    });

    d_tables.on('draw.dt', function () {
        var PageInfo = $("#" + tableName).DataTable().page.info();
        d_tables.column(0, { page: 'current' }).nodes().each(function (cell, i) {
            cell.innerHTML = '<strong>'+ (i + 1 + PageInfo.start) +'</strong>';
        });
    });

    return d_tables;
}


class Categories {
    constructor(name, description, id=''){
        this.name = name;
        this.description = description;
        this.id = id;

    }

    generateData() {
        return {
            nama_kategori: this.name,
            keterangan_kategori: this.description
        }
    }

    insertRow(url) {
        let data = this.generateData();

        axios.post(url, data).then((response) => {
            if(response.data.success == true && response.status == 200)
            {
                alertBox('BERHASIL', response.data.message, 'fa fa-check', 'green', function(){
                    modalBoxCategory.modal('hide');
                    getDatatableCategory(table_list);
                })
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    updateRow(url) {
        let data = this.generateData();
        url += '/' + this.id;

        axios.put(url, data).then(function (response) {
            console.log(response.data)
            if(response.status == 202 && response.data.success == true) {
                alertBox("BERHASIL", response.data.message, "fa fa-check", "green", function(){
                    modalBoxCategory.modal('hide');
                    getDatatableCategory(table_list);
                })
            }

            if(response.status == 200 && response.data.success == false) {
                alertBox("GAGAL", response.data.message, "fa fa-times", "red", function(){})
            }
        }).catch(function (error) {
            console.log(error.response);
        });
    }
}
