function confirmBox(title, message, icon, color, yesCall, noCall) {
    $.confirm({
        title: title,
        content: message,
        type: color,
        typeAnimated: true,
        boxWidth: '40%',
        useBootstrap: false,
        draggable: true,
        icon: icon,
        theme: 'supervan',
        buttons: {
            yes: function () {
                yesCall()
            },
            no: function () {
                noCall()
            }
        }
    });
}

function alertBox(title, message, icon, color, callback=function(){}) {
    $.alert({
        title: title,
        content: message,
        type: color,
        boxWidth: '40%',
        useBootstrap: false,
        typeAnimated: true,
        draggable: true,
        icon: icon,
        theme: 'material',
        buttons: {
            close: function () {
                callback()
            }
        }
    });
}


function tableDestroyed(tables) {
    if ($.fn.DataTable.isDataTable('#' + tables)) {
        $('#' + tables).DataTable().destroy();
    }
}
