# **A. CLIENT CONTROLLER**

### 1. KATEGORI CONTROLLER 
- Path : (Services/KategoriController)
- Method Names :

    a. findKategori [GET]
    ```json
    {
        "url": "/kategori/found/{kategori_id}",
        "response": {
            "success": true,
            "message": "Data kategori ditemukan",
            "data": {
                "id": 1,
                "nama_kategori": "PPR",
                "keterangan_kategori": "Jenis Pakaian Pria Remaja",
                "created_at": "2020-01-27 17:39:37",
                "updated_at": "2020-01-28 08:57:14"
            }
        }
    }
    ```

    b. allKategori [GET]
    ```json
    {
        "url": "/kategori/get/all",
        "response": {

            "success": true,
            "message": "Data seluruh kategori berhasil ditarik",
            "data": [
                {
                    "id": 1,
                    "nama_kategori": "PPR",
                    "keterangan_kategori": "Jenis Pakaian Pria Remaja",
                    "created_at": "2020-01-27 17:39:37",
                    "updated_at": "2020-01-28 08:57:14"
                },
                ...
            ]
        }
    }
    ```

    c. insertKategori [POST]    
    ```json
    {
        "url": "/kategori/found/{kategori_id}",
        "post": {
            "nama_kategori": "",
            "keterangan_kategori": ""
        },
        "response": {
            "success": true,
            "message": "Data kategori ditemukan",
            "data": {
                "id": 1,
                "nama_kategori": "PPR",
                "keterangan_kategori": "Jenis Pakaian Pria Remaja",
                "created_at": "2020-01-27 17:39:37",
                "updated_at": "2020-01-28 08:57:14"
            }
        }
    }
    ```

    d. updateKategori [PUT]
    ```json
    {
        "url": "/kategori/update//{kategori_id}",
        "post": {
            "nama_kategori": "",
            "keterangan_kategori": ""
        },
        "response": {
            "success":true,
            "message":"Data kategori berhasil diperbarui",
            "data":{
                "id":3,
                "nama_kategori":"PWR","keterangan_kategori":"Jenis Pakaian Wanita Remaja",
                "created_at":"2020-01-28 08:54:20","updated_at":"2020-01-28 09:25:21"
            }
        }
    }
    ```

    e. deleteKategori [DELETE]
    ```json
    {
        "url":"/kategori/delete/{kategori_id}",
        "response": {
            "success":true,
            "message":"Kategori A berhasil dihapus"
        }
    }
    ```



# **B. INSTALASI**
1. Clone Project
2. Open Project
3. Install Composer
>>
    composer install

4. Copy Environment
>>
    cp .env.example .env

5. Setting Env (Database Name And Password)
6. Set Generate Key
>>
    php artisan key:generate
        atau
    php artisan key:generate --ansi

7. Run Migration With Seeder
>> 
    php artisan migrate --seed

8. Run Serve
>>
    php artisan serve
