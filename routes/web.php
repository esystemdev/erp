<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get("/", "Auth\LoginController@showLoginForm");
Route::get("/home", "Services\ViewController@dashboard")->name('home');
Route::get("/login", "Auth\LoginController@showLoginForm");

Auth::routes();
Route::group(['middleware' => 'auth'], function () {

    //VIEW (SELURUH ROUTE VIEW)
    Route::get("/view/dashboard", "Services\ViewController@dashboard")->name('dashboard');

    Route::get("/view/kategori", "Services\ViewController@kategori");
    Route::get("/view/supplier", "Services\ViewController@supplier");
    Route::get("/view/cabang", "Services\ViewController@cabang");
    Route::get("/view/produk", "Services\ViewController@produk");


    //DATATABLE
    Route::get("/record/kategori/data", "Services\DatatableRecordController@getKategoriDatatable");
    Route::get("/record/cabang/data", "Services\DatatableRecordController@getCabangDatatable");
    Route::get("/record/supplier/data", "Services\DatatableRecordController@getKategoriDatatable");


    //KATEGORI
    Route::get("/kategori/found/{kategori}", "Services\KategoriController@findKategori");
    Route::get("/kategori/get/all", "Services\KategoriController@allKategori");
    Route::post("/kategori/insert", "Services\KategoriController@insertKategori");
    Route::put("/kategori/update/{kategori}", "Services\KategoriController@updateKategori");
    Route::delete("/kategori/delete/{kategori}", "Services\KategoriController@deleteKategori");


    //CABANG
    Route::get("/cabang", "Services\CabangController@viewCabang");
    Route::get("/cabang/found/{cabang}", "Services\CabangController@findCabang");
    Route::get("/cabang/get/all", "Services\CabangController@allCabang");
    Route::post("/cabang/insert", "Services\CabangController@insertCabang");
    Route::put("/cabang/update/{cabang}", "Services\CabangController@updateCabang");
    Route::delete("/cabang/delete/{cabang}", "Services\CabangController@deleteCabang");

});

