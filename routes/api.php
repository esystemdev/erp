<?php

use Illuminate\Http\Request;


Route::post('register', 'Api\AuthController@register');
Route::post('login', 'Api\AuthController@authenticate');
Route::get('open', 'Api\AuthController@open');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user/api', 'Api\AuthController@getAuthenticatedUser');
    Route::get('kategori/api/{kategori_id}', 'Api\KategoriApiController@getCategoryBy');
});
