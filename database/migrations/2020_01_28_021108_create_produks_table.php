<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('nama_produk');
            $table->string('kode_barang')->nullable()->default(null);
            $table->integer('id_kategori')->nullable()->default(null);
            $table->tinyInteger('jenis')->default(0); // 0=barang mentah, 1=barang jadi;
            $table->string('unit')->nullable()->default(null);
            $table->decimal('harga')->nullable()->default(null);
            $table->longText('keterangan')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produks');
    }
}
