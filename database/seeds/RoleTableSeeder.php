<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [ 'slug' => 'super_user', 'name' => 'Super User' ],
            [ 'slug' => 'admin_gudang', 'name' => 'Administrator Gudang' ],
            [ 'slug' => 'admin_toko', 'name' => 'Administrator Toko' ],
            [ 'slug' => 'cashier', 'name' => 'Kasir' ],
            [ 'slug' => 'customer', 'name' => 'Pelanggan' ],
        ];

        foreach ($roles as $role) {
            Role::create([
                'slug' => $role['slug'],
                'name' => $role['name']
            ]);
        }

    }
}
