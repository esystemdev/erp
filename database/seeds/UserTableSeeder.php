<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
use App\Models\UserProfile;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        $users = [
            [
                'fullname' => 'Super User',
                'username' => 'superuser',
                'email' => 'superuser@gmail.com',
                'password' => bcrypt('123456'),
                'roles' => 'super_user'
            ],
            [
                'fullname' => 'Administrator Gudang',
                'username' => 'admingudang',
                'email' => 'admingudang@gmail.com',
                'password' => bcrypt('123456'),
                'roles' => 'admin_gudang'
            ],
            [
                'fullname' => 'Administrator Toko',
                'username' => 'admintoko',
                'email' => 'admintoko@gmail.com',
                'password' => bcrypt('123456'),
                'roles' => 'admin_toko'
            ],
            [
                'fullname' => 'Staff Kasir',
                'username' => 'kasir',
                'email' => 'cashier@gmail.com',
                'password' => bcrypt('123456'),
                'roles' => 'cashier'
            ],

        ];

        foreach ($users as $item) {
            $user = User::create([
                'fullname' => $item['fullname'],
                'username' => $item['username'],
                'email' => $item['email'],
                'password' => $item['password'],
            ]);

            $user->roles()->attach(Role::where('slug', $item['roles'])->first());
            UserProfile::create([
                'id_user' => $user->id
            ]);
        }

    }
}
